 -- Author: 	Ryan Bramich
 -- Class:  	CMSC 430 6381
 -- Assignment:	Project 2
 -- File:	SynError2.txt
 -- Numb of errors in file: 0 - Lex, 2 - Syn 0 - Sem
 -- Error in parameters designation
 -- Error in if logic statment NOTOP given instead of binary operator.

 function main a: integer, c returns integer;
     b: integer is 3 * 2;
 begin
     if a not 0 then
         b + 3;

     else
         b * 4;
     endif;
 end;
