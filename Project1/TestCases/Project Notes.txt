Lessons Leared:
1)I tried to impliment a stack to display when multiple errors appeared on one line however the challenge with this is that there is no was to get the errors to print out in order without popping the off on stact on to a new stack and then printing and popping them off the reverse stack. because is the need for a error and reversing stact to complete this i decided to impliment a list instead to push the errors to the stack as they arrived and then retrieve and pop them from the back of the list. 

2) I spent quite a bit of time trying to troubleshoot why when I mispelled a keyword that is was not being caught as a lexical error. I discovered that it was being picked up as an ID when it was spelled wrong due to removing the ECHO in the scanner.l file so I could visually see that it the identifier was picking it up. I then verified the reverse by leaving the reserve words spelled correctly and removing the return statment for the token and after viewing the lexemes.txt file after running the program I could see that the keywords were not printing to the text file which mean they were being picked up as reserve words and not identifier's. After further investigation and reading ahead it seems that this type of reserve word spelling error should be caught in the Syntaxical or Semantical error phase and not as a lexical error due to mispelled tokens meeting the identifier token lexem. 

Test Cases:
1) test case containing all Lexemes
2) Test case with multiple errors on one line
3) test case with no Errors